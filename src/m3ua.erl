%%% m3ua.erl
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% @copyright 2015-2018 SigScale Global Inc.
%%% @end
%%% Licensed under the Apache License, Version 2.0 (the "License");
%%% you may not use this file except in compliance with the License.
%%% You may obtain a copy of the License at
%%%
%%%     http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing, software
%%% distributed under the License is distributed on an "AS IS" BASIS,
%%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%%% See the License for the specific language governing permissions and
%%% limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% @doc This library module implements the public API for the
%%% 	{@link //m3ua. m3ua} application.
%%%
-module(m3ua).
-copyright('Copyright (c) 2015-2018 SigScale Global Inc.').

%% export the m3ua public API
-export([start/1, start/3, stop/1]).
-export([sctp_release/2, sctp_status/2]).
-export([getstat/1, getstat/2, getstat/3]).
-export([as_add/6, as_delete/1, register/5, register/6]).
-export([get_ep/0, get_as/0, get_assoc/0, get_assoc/1]).
-export([asp_status/2, asp_up/2, asp_down/2, asp_active/2,
			asp_inactive/2]).
-export([transfer/8]).

%% export the m3ua private API
-export([sort/1]).

-type option() :: {name, term()}
		| {connect, inet:ip_address(), inet:port_number(), [gen_sctp:option()]}
		| {role, sgp | asp}
		| {static_keys, [{RC :: 0..4294967295, RK :: routing_key(), AS :: term()}]}
		| {use_rc, boolean()}
		| gen_sctp:option().
%% Options used to configure SCTP endpoint and M3UA process behaviour.
-export_type([option/0]).

-include("m3ua.hrl").
-include_lib("kernel/include/inet_sctp.hrl").

-type stat_option() ::
	'recv_cnt' | 'recv_max' | 'recv_avg' | 'recv_oct' | 'recv_dvi' |
	'send_cnt' | 'send_max' | 'send_avg' | 'send_oct' | 'send_pend'.

%%----------------------------------------------------------------------
%%  The m3ua public API
%%----------------------------------------------------------------------

-spec start(Callback) -> Result
	when
		Callback :: atom() | #m3ua_fsm_cb{},
		Result :: {ok, EndPoint} | {error, Reason},
		EndPoint :: pid(),
		Reason :: term().
%% @equiv start(Callback, 0, [])
start(Callback) ->
	start(Callback, 0, []).

-spec start(Callback, Port, Options) -> Result
	when
		Port :: inet:port_number(),
		Options :: [option()],
		Callback :: atom() | #m3ua_fsm_cb{},
		Result :: {ok, EndPoint} | {error, Reason},
		EndPoint :: pid(),
		Reason :: term().
%% @doc Start an M3UA service on a new SCTP endpoint.
%%
%% 	Default options create an endpoint for an M3UA
%% 	Signaling Gateway Process (SGP) in server mode.
%%
start(Callback, Port, Options) when is_integer(Port), is_list(Options),
		((Callback == false) orelse is_atom(Callback) orelse is_tuple(Callback)) ->
	m3ua_lm_server:start(Callback, [{port, Port} | Options]).

-spec stop(EndPoint:: pid()) -> ok | {error, Reason :: term()}.
%% @doc Close a previously opened endpoint.
stop(EP) when is_pid(EP) ->
	m3ua_lm_server:stop(EP).

-spec as_add(Name, NA, Keys, Mode, MinASP, MaxASP) -> Result
	when
		Name :: term(),
		NA :: undefined | pos_integer(),
		Keys :: [Key],
		MinASP :: pos_integer(),
		MaxASP :: pos_integer(),
		Key :: {DPC, [SI], [OPC]},
		DPC :: pos_integer(),
		SI :: pos_integer(),
		OPC :: pos_integer(),
		Mode :: overide | loadshare | broadcast,
		Result :: {ok, AS} | {error, Reason},
		AS :: #m3ua_as{},
		Reason :: term().
%% @doc Add an Application Server (AS).
as_add(Name, NA, Keys, Mode, MinASP, MaxASP)
		when ((NA == undefined) orelse is_integer(NA)),
		is_list(Keys), is_atom(Mode),
		is_integer(MinASP), is_integer(MaxASP) ->
	m3ua_lm_server:as_add(Name, NA, Keys, Mode, MinASP, MaxASP).

-spec as_delete(RoutingKey) -> Result
	when
		RoutingKey :: {NA, Keys, Mode},
		NA :: undefined | pos_integer(),
		Keys :: [Key],
		Key :: {DPC, [SI], [OPC]},
		DPC :: pos_integer(),
		SI :: pos_integer(),
		OPC :: pos_integer(),
		Mode :: overide | loadshare | broadcast,
		Result :: ok | {error, Reason},
		Reason :: term().
%% @doc Delete an Application Server (AS).
as_delete(RoutingKey) ->
	m3ua_lm_server:as_delete(RoutingKey).

-spec getstat(EndPoint) -> Result
	when
		EndPoint :: pid(),
		Result :: {ok, OptionValues} | {error, inet:posix()},
		OptionValues :: [{stat_option(), Count}],
		Count :: non_neg_integer().
%% @doc Get all socket statistics for an endpoint.
getstat(EndPoint) when is_pid(EndPoint) ->
	gen_fsm:sync_send_all_state_event(EndPoint, {getstat, undefined}).

-spec getstat(EndPoint, AssocOrOptions) -> Result
	when
		EndPoint :: pid(),
		AssocOrOptions :: Assoc | Options,
		Assoc :: gen_sctp:assoc_id(),
		Options :: [stat_option()],
		Result :: {ok, OptionValues} | {error, inet:posix()},
		OptionValues :: [{stat_option(), Count}],
		Count :: non_neg_integer().
%% @doc Get socket statistics.
%%
%% 	Get socket statistics for an endpoint with `Options'
%% 	specifying the specific statistics to retrieve or
%% 	get all socket statistics for the association `Assoc'.
%%
getstat(EndPoint, Options)
		when is_pid(EndPoint), is_list(Options)  ->
	gen_fsm:sync_send_all_state_event(EndPoint, {getstat, Options});
getstat(EndPoint, Assoc)
		when is_pid(EndPoint), is_integer(Assoc) ->
	m3ua_lm_server:getstat(EndPoint, Assoc).

-spec getstat(EndPoint, Assoc, Options) -> Result
	when
		EndPoint :: pid(),
		Assoc :: gen_sctp:assoc_id(),
		Options :: [stat_option()],
		Result :: {ok, OptionValues} | {error, inet:posix()},
		OptionValues :: [{stat_option(), Count}],
		Count :: non_neg_integer().
%% @doc Get specific socket statistics for an association.
getstat(EndPoint, Assoc, Options)
		when is_pid(EndPoint), is_integer(Assoc), is_list(Options)  ->
	m3ua_lm_server:getstat(EndPoint, Assoc, Options).

-spec register(EndPoint, Assoc, NA, Keys, Mode) -> Result
	when
		EndPoint :: pid(),
		Assoc :: gen_sctp:assoc_id(),
		NA :: pos_integer(),
		Keys :: [Key],
		Key :: {DPC, [SI], [OPC]},
		DPC :: pos_integer(),
		SI :: pos_integer(),
		OPC :: pos_integer(),
		Mode :: overide | loadshare | broadcast,
		Result :: {ok, RoutingContext} | {error, Reason},
		RoutingContext :: pos_integer(),
		Reason :: term().
%% @equiv register(EndPoint, Assoc, NA, Keys, Mode, undefined)
register(EndPoint, Assoc, NA, Keys, Mode)
		when is_pid(EndPoint), is_integer(Assoc), is_list(Keys),
		((NA == undefined) or is_integer(NA)),
		((Mode == overide) orelse (Mode == loadshare)
		orelse (Mode == broadcast)) ->
	register(EndPoint, Assoc, NA, Keys, Mode, undefined).

-spec register(EndPoint, Assoc, NA, Keys, Mode, AsName) -> Result
	when
		EndPoint :: pid(),
		Assoc :: gen_sctp:assoc_id(),
		NA :: pos_integer(),
		Keys :: [Key],
		Key :: {DPC, [SI], [OPC]},
		DPC :: pos_integer(),
		SI :: pos_integer(),
		OPC :: pos_integer(),
		Mode :: overide | loadshare | broadcast,
		AsName :: term(),
		Result :: {ok, RoutingContext} | {error, Reason},
		RoutingContext :: pos_integer(),
		Reason :: term().
%% @doc Register a routing key for an application server.
register(EndPoint, Assoc, NA, Keys, Mode, AsName)
		when is_pid(EndPoint), is_integer(Assoc), is_list(Keys),
		((NA == undefined) or is_integer(NA)),
		((Mode == overide) orelse (Mode == loadshare)
		orelse (Mode == broadcast)) ->
	m3ua_lm_server:register(EndPoint, Assoc, NA, Keys, Mode, AsName).

-spec sctp_release(EndPoint, Assoc) -> Result
	when
		EndPoint :: pid(),
		Assoc :: gen_sctp:assoc_id(),
		Result :: ok | {error, Reason},
		Reason :: term().
%% @doc Release an established SCTP association.
sctp_release(EndPoint, Assoc) ->
	m3ua_lm_server:sctp_release(EndPoint, Assoc).

-spec sctp_status(EndPoint, Assoc) -> Result
	when
		EndPoint :: pid(),
		Assoc :: gen_sctp:assoc_id(),
		Result :: {ok, AssocStatus} | {error, Reason},
		AssocStatus :: #sctp_status{},
		Reason :: term().
%% @doc Report the status of an SCTP association.
sctp_status(EndPoint, Assoc) ->
	m3ua_lm_server:sctp_status(EndPoint, Assoc).

-spec asp_status(EndPoint, Assoc) -> AspState
	when
		EndPoint :: pid(),
		Assoc :: gen_sctp:assoc_id(),
		AspState :: down | inactive | active.
%% @doc Report the status of local or remote ASP.
asp_status(EndPoint, Assoc) ->
	m3ua_lm_server:asp_status(EndPoint, Assoc).

%%-spec as_status(SAP, ???) -> Result
%%	when
%%		SAP :: pid(),
%%		Result :: {ok, AsState} | {error, Reason},
%%		AsState :: down | inactive | active | pending,
%%		Reason :: term().
%%%% doc Report the status of an AS.
%%as_status(SAP, ???) ->
%%	todo.

-spec asp_up(EndPoint, Assoc) -> Result
	when
		EndPoint :: pid(),
		Assoc :: gen_sctp:assoc_id(),
		Result :: ok | {error, Reason},
		Reason :: asp_not_found | term().
%% @doc Requests that ASP start its operation
%%  and send an ASP Up message to its peer.
asp_up(EndPoint, Assoc) ->
	m3ua_lm_server:asp_up(EndPoint, Assoc).

-spec asp_down(EndPoint, Assoc) -> Result
	when
		EndPoint :: pid(),
		Assoc :: gen_sctp:assoc_id(),
		Result :: ok | {error, Reason},
		Reason :: asp_not_found | term().
%% @doc Requests that ASP stop its operation
%%  and send an ASP Down message to its peer.
asp_down(EndPoint, Assoc) ->
	m3ua_lm_server:asp_down(EndPoint, Assoc).

-spec asp_active(EndPoint, Assoc) -> Result
	when
		EndPoint :: pid(),
		Assoc :: gen_sctp:assoc_id(),
		Result :: ok | {error, Reason},
		Reason :: asp_not_found | term().
%% @doc Requests that ASP send an ASP Active message to its peer.
asp_active(EndPoint, Assoc) ->
	m3ua_lm_server:asp_active(EndPoint, Assoc).

-spec asp_inactive(EndPoint, Assoc) -> Result
	when
		EndPoint :: pid(),
		Assoc :: gen_sctp:assoc_id(),
		Result :: ok | {error, Reason},
		Reason :: asp_not_found | term().
%% @doc Requests that ASP send an ASP Inactive message to its peer.
asp_inactive(EndPoint, Assoc) ->
	m3ua_lm_server:asp_inactive(EndPoint, Assoc).

-spec transfer(Fsm, Stream, OPC, DPC, NI, SI, SLS, Data) -> Result
	when
		Fsm :: pid(),
		Stream :: pos_integer(),
		OPC :: 0..4294967295,
		DPC :: 0..4294967295,
		NI :: byte(),
		SI :: byte(),
		SLS :: byte(),
		Data :: binary(),
		Result :: ok | {error, Reason},
		Reason :: term().
%% @doc MTP-TRANSFER request.
%%
%% Called by an MTP user to transfer data using the MTP service.
transfer(Fsm, Stream, OPC, DPC, NI, SI, SLS, Data)
		when is_pid(Fsm), is_integer(Stream), Stream =/= 0,
		is_integer(OPC), is_integer(DPC), is_integer(NI),
		is_integer(SI), is_integer(SLS), is_binary(Data) ->
	Params = {Stream, OPC, DPC, NI, SI, SLS, Data},
	gen_fsm:sync_send_event(Fsm, {'MTP-TRANSFER', request, Params}).

-spec get_as() -> Result
	when
		Result :: {ok, [AS]} | {error, Reason},
		AS :: {Name, NA, Keys, TMT, MinASP, MaxASP, State},
		Name :: string(),
		NA :: pos_integer(),
		Keys :: [key()],
		TMT :: tmt(),
		MinASP :: pos_integer(),
		MaxASP :: pos_integer(),
		State :: down | inactive | active | pending,
		Reason :: term().
%% @doc Get all Application Servers (AS).
%%
get_as() ->
	Fold = fun(#m3ua_as{routing_key = {NA, Keys, TMT}, name = Name,
				min_asp = MinASP, max_asp = MaxASP, state = State}, Acc) ->
				[{Name, NA, Keys, TMT, MinASP, MaxASP, State} | Acc] 
	end,
	case mnesia:transaction(fun() -> mnesia:foldl(Fold, [], m3ua_as) end) of
		{atomic, ASs} ->
			{ok, ASs};
		{aborted, Reason} ->
			{error, Reason}
	end.

-spec get_ep() -> Result
	when
		Result :: [EP],
		EP :: pid().
%% @doc Get all SCTP endpoints on local node.
%%
get_ep() ->
	get_ep(get_ep_sups(), []).
%% @hidden
get_ep([H | T], Acc) ->
	EP = find_ep(supervisor:which_children(H)),
	get_ep(T, [EP | Acc]);
get_ep([], Acc) ->
	lists:reverse(Acc).

-spec get_assoc() -> Result
	when
		Result :: [{EP, Assoc}],
		EP :: pid(),
		Assoc :: gen_sctp:assoc_id().
%% @doc Get all SCTP associations.
%%
get_assoc() ->
	get_assoc(get_ep_sups(), []).
%% @hidden
get_assoc([H | T], Acc) ->
	EP = find_ep(supervisor:which_children(H)),
	L = [{EP, Assoc} || Assoc <- get_assoc(EP)],
	get_assoc(T, [L | Acc]);
get_assoc([], Acc) ->
	lists:flatten(lists:reverse(Acc)).

-spec get_assoc(EP) -> Result
	when
		EP :: pid(),
		Result :: [Assoc],
		Assoc :: gen_sctp:assoc_id().
%% @doc Get SCTP associations on local endpoint.
%%
get_assoc(EP) when is_pid(EP) ->
	gen_fsm:sync_send_all_state_event(EP, getassoc).

%%----------------------------------------------------------------------
%%  The m3ua private API
%%----------------------------------------------------------------------

-spec sort(Keys) -> Keys
	when
		Keys :: [{DPC, [SI], [OPC]}],
		DPC :: pos_integer(),
		SI :: pos_integer(),
		OPC :: pos_integer().
%% @doc Uniquely sort list of routing keys.
%% @private
sort(Keys) when is_list(Keys) ->
	sort(Keys, []).
%% @hidden
sort([{DPC, SIs, OPCs} | T], Acc) when is_integer(DPC) ->
	SortedSIs = lists:sort(SIs),
	SortedOPCs = lists:sort(OPCs),
	sort(T, [{DPC, SortedSIs, SortedOPCs} | Acc]);
sort([], Acc) ->
	lists:sort(Acc).

%%----------------------------------------------------------------------
%%  internal functions
%%----------------------------------------------------------------------

%% @hidden
get_ep_sups() ->
	get_ep_sups(whereis(m3ua_sup)).
%% @hidden
get_ep_sups(TopSup) when is_pid(TopSup) ->
	Children1 = supervisor:which_children(TopSup),
	{_, Sup2,  _, _} = lists:keyfind(m3ua_endpoint_sup_sup, 1, Children1),
	[S || {_, S, _, _} <- supervisor:which_children(Sup2)];
get_ep_sups(undefined) ->
	[].

%% @hidden
find_ep([{m3ua_listen_fsm, EP, _, _} | _]) ->
	EP;
find_ep([{m3ua_connect_fsm, EP, _, _} | _]) ->
	EP;
find_ep([_ | T]) ->
	find_ep(T).

